﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

using System.Collections.Generic;
namespace final_plataforma.Models
{

    public class Usuario
    {
        public int id { get; set; }
        public int dni { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string mail { get; set; }
        public string password { get; set; }
        public string tipo { get; set; }
        public string cuil { get; set; }

        public int id_carro { get; set; }

        public bool habilitado {get;set;}
        public int intentos { get; set; }
        public Carro carro { get; set; }

        public List<Usuario_Compra> usuario_compra { get; set; }

        public Usuario()
        {
        }

        public string toString()
        {
            return "Usuario: " + this.id + " - Dni " + this.dni + " - Nombre " + this.nombre + " - Apellido " + this.apellido + " - Mail " + this.mail + " - Cuil/Cuit:" + this.cuil + " - Tipo de Usuario:" + this.tipo; // " - Carro :" + MiCarro.toString();
        }
    }
}