﻿using System;
using System.Collections.Generic;
using System.Text;

namespace final_plataforma.Models
{
   public class Producto_Compra
    {
        public int id { get; set; }
        public int id_compra { get; set;}
        public int id_producto  { get; set;}
        public int cantidad { get; set; }
        public Compra compra { get; set; }
        public Producto producto { get; set; }


        public Producto_Compra() { }
    }
}
