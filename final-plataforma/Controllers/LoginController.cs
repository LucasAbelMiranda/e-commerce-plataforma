﻿using final_plataforma.Data;
using final_plataforma.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace final_plataforma.Controllers
{
    public class LoginController : Controller
    {
        private readonly final_plataformaContext _context;
        private readonly string admin = "admin";
       

        public LoginController(final_plataformaContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login([Bind("dni,password")] Usuario usuarioLogin)
        {
            HttpContext.Session.Remove("id_usuario");
            HttpContext.Session.Remove("id_admin");

            Usuario usuario = new Usuario();


            if (ModelState.IsValid)
            {
                //TODO cambiar este metodo a async
                usuario = _context.usuarios.Where(U => (U.dni == usuarioLogin.dni && U.password == usuarioLogin.password)).FirstOrDefault();

                //el usuario coloco mal el password, busco nuevamente el usuario y actualizo el mail //el dni puede estar repetido? TODO
                if (usuario == null)
                {
                    usuario = _context.usuarios.Where(U => (U.dni == usuarioLogin.dni)).FirstOrDefault();
                    if (usuario != null)
                    {

                        if (usuario.intentos > 0)
                        {
                            usuario.intentos--;
                        }
                        else
                        {
                            TempData["bloqueado"] = "true";
                            usuario.habilitado = false;
                        }

                        _context.usuarios.Update(usuario);
                        _context.SaveChanges();
                    }
                    return RedirectToAction(nameof(Index));
                }

                //el usuario es admin y se lo redirecciona a la pantalla de administracion de productos
                if (usuario != null && usuario.habilitado && usuario.tipo == admin)
                {
                    HttpContext.Session.SetString("id_admin", usuario.id.ToString());
                    return RedirectToAction("Index", "Productos");
                }

                //el usuario no es admin se lo redirecciona al mercado
                if (usuario != null && usuario.habilitado)
                {
                    TempData["usuarioLogueado"] = usuario.id.ToString(); //se pasa el usuario logueado como data temporal
                    HttpContext.Session.SetString("id_usuario", usuario.id.ToString());

                    return RedirectToAction("Index", "Mercado");
                }

            }
            return RedirectToAction(nameof(Index));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
