﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using final_plataforma.Data;
using final_plataforma.Models;
using Microsoft.AspNetCore.Http;

namespace final_plataforma.Controllers
{
    public class ComprasController : Controller
    {
        private readonly final_plataformaContext _context;

        public ComprasController(final_plataformaContext context)
        {
            _context = context;
        }

        // GET: Compras
        public async Task<IActionResult> Index()
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            // _context.producto_compra.Load();
            // _context.compras.Load();
            _context.producto.Load();
            _context.usuarios.Load();
            _context.usuario_compra.Load();
            var lista = await _context.compras.Include(C => C.producto_compra).ToListAsync();
            return View(lista);
        }

        // GET: Compras/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var compra = await _context.compras
                .FirstOrDefaultAsync(m => m.id == id);
            if (compra == null)
            {
                return NotFound();
            }

            return View(compra);
        }

        // GET: Compras/Create
        public IActionResult Create()
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }

        // POST: Compras/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,total")] Compra compra)
        {
            if (ModelState.IsValid)
            {
                _context.Add(compra);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(compra);
        }

        // GET: Compras/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var compra = await _context.compras.FindAsync(id);
            if (compra == null)
            {
                return NotFound();
            }
            return View(compra);
        }

        // POST: Compras/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,total")] Compra compra)
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            if (id != compra.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(compra);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompraExists(compra.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(compra);
        }

        // GET: Compras/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var compra = await _context.compras
                .FirstOrDefaultAsync(m => m.id == id);
            if (compra == null)
            {
                return NotFound();
            }

            return View(compra);
        }

        // POST: Compras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var compra = await _context.compras.FindAsync(id);
            _context.compras.Remove(compra);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompraExists(int id)
        {
            return _context.compras.Any(e => e.id == id);
        }
    }
}
