﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using final_plataforma.Data;
using final_plataforma.Models;
using Microsoft.AspNetCore.Http;

namespace final_plataforma.Controllers
{

    //ver como pasar el usuario entre pantallas
    public class MercadoController : Controller
    {
        private readonly final_plataformaContext _context;
        private int idUsuario;

        public MercadoController(final_plataformaContext context)
        {
            _context = context;
        }

        // pantalla principal donde se muestran los productos
        public IActionResult Index(string sortOrder, string nombreProd, string nombreCateg)
        {

    
            string id = HttpContext.Session.GetString("id_usuario");

            if (id==null)
            {
                return RedirectToAction("Index", "Login");
            }

            Usuario usuario = _context.usuarios.Where(U => U.id == Int32.Parse(id)).Include(U => U.carro).Include(U=>U.carro.producto_Carro).FirstOrDefault();

            //ver como cargar esta lista porque producto carro es una lista
            _context.producto.Load();

            ViewBag.nombre = usuario.nombre;

            List<int> productosEnCarro = new List<int>();
            if (usuario.carro!=null && usuario.carro.producto_Carro != null)
            {
                foreach (Producto_Carro PC in usuario.carro.producto_Carro)
                {
                    productosEnCarro.Add(PC.id_Producto);
                }
            }
            
            ViewBag.productosEnCarro = productosEnCarro.ToList();
            ViewBag.cantProductos = productosEnCarro.Count();


            ViewBag.NombreSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PrecioSortParm = sortOrder == "Precio" ? "Precio_desc" : "Precio";
            ViewBag.CantidadSortParm = sortOrder == "Cantidad" ? "cantidad_desc" : "Cantidad";
            ViewBag.CategoriaSortParm = sortOrder == "Categoria" ? "categoria_desc" : "Categoria";

            var productos = _context.producto.Include(p => p.categoria);
            var listaProductos = productos.Where(P => P.cantidad > 0);

            if (!String.IsNullOrEmpty(nombreProd))
            {
                listaProductos = listaProductos.Where(s => s.nombre.Contains(nombreProd));
            }

            if (!String.IsNullOrEmpty(nombreCateg))
            {
                listaProductos = listaProductos.Where(s => s.categoria.nombre.Contains(nombreCateg));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    listaProductos = listaProductos.OrderByDescending(s => s.nombre);
                    break;
                case "Precio":
                    listaProductos = listaProductos.OrderBy(s => s.precio);
                    break;
                case "Precio_desc":
                    listaProductos = listaProductos.OrderByDescending(s => s.precio);
                    break;
                case "Cantidad":
                    listaProductos = listaProductos.OrderBy(s => s.cantidad);
                    break;
                case "cantidad_desc":
                    listaProductos = listaProductos.OrderByDescending(s => s.cantidad);
                    break;
                case "Categoria":
                    listaProductos = listaProductos.OrderBy(s => s.categoria.nombre);
                    break;
                case "categoria_desc":
                    listaProductos = listaProductos.OrderByDescending(s => s.categoria.nombre);
                    break;
                default:
                    listaProductos = listaProductos.OrderBy(s => s.nombre);
                    break;
            }
            return View(listaProductos);
        }

        public IActionResult Success()
        {
            string id = HttpContext.Session.GetString("id_usuario");

            if (id == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }

        public IActionResult Error()
        {
            string id = HttpContext.Session.GetString("id_usuario");

            if (id == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }

        //vista de carrito
        public IActionResult Carrito()
        {
            string id = HttpContext.Session.GetString("id_usuario");

            if (id == null)
            {
                return RedirectToAction("Index", "Login");
            }

            Int32.TryParse(id, out idUsuario);

            _context.producto.Load();
            Usuario usuario = _context.usuarios.Where(U => U.id == Int32.Parse(id)).Include(U => U.carro).Include(U => U.carro.producto_Carro).FirstOrDefault();



            if (usuario.carro.producto_Carro == null)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(usuario.carro);
        }

        //vista mis datos
        public async Task<IActionResult> compras()
        {
            string id = HttpContext.Session.GetString("id_usuario");

            if (id == null)
            {
                return RedirectToAction("Index", "Login");
            }
   

            _context.producto.Load();
            return View(await _context.compras  .Where(C => C.usuario_compra.Any(UC => UC.id_usuario == Int32.Parse(id)))
                                                .Include(C => C.producto_compra)
                                                .ToListAsync());

        }


        public async Task<IActionResult> Cuenta()
        {


            string id = HttpContext.Session.GetString("id_usuario");

            if (id == null)
            {
                return RedirectToAction("Index", "Login");
            }
            Int32.TryParse(id, out idUsuario);

            var usuario = await _context.Usuario.FindAsync(idUsuario);
            if (usuario == null)
            {
                return NotFound();
            }
            return View(usuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Guardar(int id, [Bind("id,dni,nombre,apellido,mail,password,tipo,cuil,id_carro")] Usuario usuario,string nuevoPassword, string nuevoPassword2)
        {
            if (id != usuario.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (!validarPassword(usuario.password, nuevoPassword, nuevoPassword2))
                    {
                        return RedirectToAction(nameof(Cuenta));
                    }
                    usuario.habilitado = true;
                    usuario.intentos = 3;
                    usuario.password = nuevoPassword;
                    _context.Update(usuario);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UsuarioExists(usuario.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Cuenta));
        }

        private bool validarPassword(string password, string nuevoPassword, string nuevoPassword2)
        {
            if (password== null || password.Length == 0) return false;
            if (nuevoPassword == null || nuevoPassword.Length == 0) return false;
            if (nuevoPassword != nuevoPassword2) return false;
            return true;
        }

        [HttpPost, ActionName("Agregar")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Agregar(int id)
        {
            //session usuario
            string id1 = HttpContext.Session.GetString("id_usuario");
            Int32.TryParse(id1, out idUsuario);

            var usuario = await _context.Usuario.FindAsync(idUsuario);

            Producto_Carro producto_Carro = _context.producto_carro.Where(C => (C.id_Carro == usuario.id_carro && C.id_Producto == id)).FirstOrDefault();

            if (producto_Carro == null)
            {
                //se agrega un producto
                producto_Carro = new Producto_Carro { id_Producto = id, cantidad = 1, id_Carro = usuario.id_carro };
                _context.producto_carro.Add(producto_Carro);
                _context.SaveChanges();
            }

            return RedirectToAction(nameof(Carrito));
        }

        private bool UsuarioExists(int id)
        {
            return _context.Usuario.Any(e => e.id == id);
        }

        //salir
        public async Task<IActionResult> Salir()
        {
            //context.dispose
            var final_plataformaContext = _context.producto.Include(p => p.categoria);
            return View(await final_plataformaContext.ToListAsync());
        }


        /*agregar y quitar items del carrito*/

        [HttpPost, ActionName("agregaritem")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> addItem(int id)
        {
            Producto_Carro pc = _context.producto_carro.Where(PC => (PC.id == id)).FirstOrDefault();
            Producto p = _context.producto.Where(P => (P.id == pc.id_Producto)).FirstOrDefault();

            if (pc.cantidad + 1 <= p.cantidad)
            {
                pc.cantidad++;
                _context.producto_carro.Update(pc);
                _context.SaveChanges();
            }

            return RedirectToAction(nameof(Carrito));
        }

        [HttpPost, ActionName("quitaritem")]
        [ValidateAntiForgeryToken]
        public IActionResult removeItem(int id)
        {

            Producto_Carro pc = _context.producto_carro.Where(PC => (PC.id == id)).FirstOrDefault();

            if (pc.cantidad - 1 == 0)
            {
                _context.producto_carro.Remove(pc);
                _context.SaveChanges();
            }
            else
            {
                pc.cantidad--;
                _context.producto_carro.Update(pc);
                _context.SaveChanges();
            }

            return RedirectToAction(nameof(Carrito));
        }

        [HttpPost, ActionName("vaciar")]
        [ValidateAntiForgeryToken]
        public IActionResult vaciar(int id)
        {

            _context.producto_carro.RemoveRange(_context.producto_carro.Where(PC => PC.id_Carro == id));
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }



        [HttpPost, ActionName("comprar")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> comprar(int id)
        {
            double total = 0;
           _context.producto.Load();


            Carro c = _context.carro.Where(C => (C.id == id))
                                    .Include(C => C.producto_Carro)
                                    .FirstOrDefault();
            bool flag = true;
            foreach (var producto_carro in c.producto_Carro)
            {
                //hay existencias de ese producto en la tabla de producto?
                Producto producto = _context.producto.Where(P => (P.id == producto_carro.id_Producto)).FirstOrDefault();
                if (producto.cantidad >= producto_carro.cantidad)
                {
                    total += producto_carro.cantidad * producto_carro.producto.precio;
                    producto.cantidad = producto.cantidad - producto_carro.cantidad;
                    _context.producto.Update(producto);
                }
                else
                {
                    flag = false;
                    if (producto.cantidad > 0)
                    {
                        producto_carro.cantidad = producto.cantidad;
                        _context.producto_carro.Update(producto_carro);
                    }
                    else
                    {
                        _context.producto_carro.Remove(producto_carro);
                    }
                   
                }

            }
            _context.SaveChanges();

            if (flag)
            {

                //TODO REVISAR
                _context.usuario_compra.Load();

                Compra compra = new Compra { total = total };


                _context.compras.Add(compra);

                _context.SaveChanges();

                foreach (var producto_carro in c.producto_Carro)
                {
                    Producto_Compra p_co = new Producto_Compra { id_compra = compra.id, id_producto = producto_carro.producto.id, cantidad = producto_carro.cantidad };
                    _context.producto_compra.Add(p_co);

                }
                _context.SaveChanges();


                Usuario_Compra usuario_compra = new Usuario_Compra { id_usuario = c.usuario_id, id_compra = compra.id };
                _context.usuario_compra.Add(usuario_compra);

                _context.SaveChanges();

                vaciar(id);

                return RedirectToAction(nameof(Success));
            }

            return RedirectToAction(nameof(Error));
            
        }

        private bool LoginOK()
        {
            return (HttpContext.Session.GetString("id_usuario") != null);
        }

    }
}
