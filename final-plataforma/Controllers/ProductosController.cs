﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using final_plataforma.Data;
using final_plataforma.Models;
using Microsoft.AspNetCore.Http;

namespace final_plataforma.Controllers
{
    public class ProductosController : Controller
    {
        private readonly final_plataformaContext _context;

        public ProductosController(final_plataformaContext context)
        {
            _context = context;
        }

        // GET: Productoes
        public async Task<IActionResult> Index()
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var final_plataformaContext = _context.producto.Include(p => p.categoria);
            return View(await final_plataformaContext.ToListAsync());
        }

        // GET: Productoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var producto = await _context.producto
                .Include(p => p.categoria)
                .FirstOrDefaultAsync(m => m.id == id);
            if (producto == null)
            {
                return NotFound();
            }

            return View(producto);
        }

        // GET: Productoes/Create
        public IActionResult Create()
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            ViewData["id_categoria"] = new SelectList(_context.categorias, "id", "id");
            return View();
        }

        // POST: Productoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,nombre,precio,cantidad,id_categoria")] Producto producto)
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            if (ModelState.IsValid)
            {
                _context.Add(producto);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["id_categoria"] = new SelectList(_context.categorias, "id", "id", producto.id_categoria);
            return View(producto);
        }

        // GET: Productoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var producto = await _context.producto.FindAsync(id);
            if (producto == null)
            {
                return NotFound();
            }
            ViewData["id_categoria"] = new SelectList(_context.categorias, "id", "id", producto.id_categoria);
            return View(producto);
        }

        // POST: Productoes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,nombre,precio,cantidad,id_categoria")] Producto producto)
        {
            if (id != producto.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(producto);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductoExists(producto.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["id_categoria"] = new SelectList(_context.categorias, "id", "id", producto.id_categoria);
            return View(producto);
        }

        // GET: Productoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            string idAdmin = HttpContext.Session.GetString("id_admin");

            if (idAdmin == null)
            {
                return RedirectToAction("Index", "Login");
            }
            if (id == null)
            {
                return NotFound();
            }

            var producto = await _context.producto
                .Include(p => p.categoria)
                .FirstOrDefaultAsync(m => m.id == id);
            if (producto == null)
            {
                return NotFound();
            }

            return View(producto);
        }

        // POST: Productoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var producto = await _context.producto.FindAsync(id);
            _context.producto.Remove(producto);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductoExists(int id)
        {
            return _context.producto.Any(e => e.id == id);
        }
    }
}
