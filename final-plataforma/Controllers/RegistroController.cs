﻿using final_plataforma.Data;
using final_plataforma.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace final_plataforma.Controllers
{
    public class RegistroController : Controller
    {
        private readonly final_plataformaContext _context;


        public RegistroController(final_plataformaContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registrar([Bind("id,dni,nombre,apellido,mail,password,tipo,cuil,id_carro,habilitado")] Usuario usuario)
        {

            if (ModelState.IsValid)
            {
                Carro carro = new Carro();
                usuario.carro = carro;
                _context.Add(carro);
                usuario.habilitado = true; //comentar para que deba ser habilitado por un admin
                usuario.intentos = 3;
                _context.Add(usuario);
                await _context.SaveChangesAsync();
                usuario.id_carro = carro.id;
                _context.usuarios.Update(usuario);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Home");
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
