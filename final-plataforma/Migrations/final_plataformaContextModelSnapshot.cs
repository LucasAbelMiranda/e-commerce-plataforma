﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using final_plataforma.Data;

namespace final_plataforma.Migrations
{
    [DbContext(typeof(final_plataformaContext))]
    partial class final_plataformaContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.21")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("final_plataforma.Models.Carro", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("usuario_id")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("usuario_id")
                        .IsUnique();

                    b.ToTable("carro");

                    b.HasData(
                        new
                        {
                            id = 1,
                            usuario_id = 1
                        },
                        new
                        {
                            id = 2,
                            usuario_id = 2
                        });
                });

            modelBuilder.Entity("final_plataforma.Models.Categoria", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("nombre")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("id");

                    b.ToTable("categorias");

                    b.HasData(
                        new
                        {
                            id = 1,
                            nombre = "electro"
                        },
                        new
                        {
                            id = 2,
                            nombre = "deco"
                        },
                        new
                        {
                            id = 3,
                            nombre = "varios"
                        });
                });

            modelBuilder.Entity("final_plataforma.Models.Compra", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<double>("total")
                        .HasColumnType("float");

                    b.HasKey("id");

                    b.ToTable("compras");
                });

            modelBuilder.Entity("final_plataforma.Models.Producto", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("cantidad")
                        .HasColumnType("int");

                    b.Property<int>("id_categoria")
                        .HasColumnType("int");

                    b.Property<string>("nombre")
                        .HasColumnType("nvarchar(max)");

                    b.Property<double>("precio")
                        .HasColumnType("float");

                    b.HasKey("id");

                    b.HasIndex("id_categoria");

                    b.ToTable("producto");

                    b.HasData(
                        new
                        {
                            id = 1,
                            cantidad = 200,
                            id_categoria = 1,
                            nombre = "tv",
                            precio = 100.0
                        },
                        new
                        {
                            id = 2,
                            cantidad = 300,
                            id_categoria = 3,
                            nombre = "radio",
                            precio = 150.0
                        },
                        new
                        {
                            id = 3,
                            cantidad = 200,
                            id_categoria = 2,
                            nombre = "silla",
                            precio = 100.0
                        });
                });

            modelBuilder.Entity("final_plataforma.Models.Producto_Carro", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("cantidad")
                        .HasColumnType("int");

                    b.Property<int>("id_Carro")
                        .HasColumnType("int");

                    b.Property<int>("id_Producto")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("id_Carro");

                    b.HasIndex("id_Producto");

                    b.ToTable("producto_carro");
                });

            modelBuilder.Entity("final_plataforma.Models.Producto_Compra", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("cantidad")
                        .HasColumnType("int");

                    b.Property<int>("id_compra")
                        .HasColumnType("int");

                    b.Property<int>("id_producto")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("id_compra");

                    b.HasIndex("id_producto");

                    b.ToTable("producto_compra");
                });

            modelBuilder.Entity("final_plataforma.Models.Usuario", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("apellido")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("cuil")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("dni")
                        .HasColumnType("int");

                    b.Property<bool>("habilitado")
                        .HasColumnType("bit");

                    b.Property<int>("id_carro")
                        .HasColumnType("int");

                    b.Property<int>("intentos")
                        .HasColumnType("int");

                    b.Property<string>("mail")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("nombre")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("password")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("tipo")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("id");

                    b.ToTable("usuarios");

                    b.HasData(
                        new
                        {
                            id = 1,
                            apellido = "apellido",
                            cuil = "123",
                            dni = 123,
                            habilitado = true,
                            id_carro = 1,
                            intentos = 3,
                            mail = "mail",
                            nombre = "cliente",
                            password = "pass",
                            tipo = "cliente"
                        },
                        new
                        {
                            id = 2,
                            apellido = "apellido",
                            cuil = "423",
                            dni = 321,
                            habilitado = true,
                            id_carro = 2,
                            intentos = 3,
                            mail = "mail",
                            nombre = "admin",
                            password = "pass",
                            tipo = "admin"
                        });
                });

            modelBuilder.Entity("final_plataforma.Models.Usuario_Compra", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("id_compra")
                        .HasColumnType("int");

                    b.Property<int>("id_usuario")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("id_compra");

                    b.HasIndex("id_usuario");

                    b.ToTable("usuario_compra");
                });

            modelBuilder.Entity("final_plataforma.Models.Carro", b =>
                {
                    b.HasOne("final_plataforma.Models.Usuario", "usuario")
                        .WithOne("carro")
                        .HasForeignKey("final_plataforma.Models.Carro", "usuario_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("final_plataforma.Models.Producto", b =>
                {
                    b.HasOne("final_plataforma.Models.Categoria", "categoria")
                        .WithMany("productos")
                        .HasForeignKey("id_categoria")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("final_plataforma.Models.Producto_Carro", b =>
                {
                    b.HasOne("final_plataforma.Models.Carro", "carro")
                        .WithMany("producto_Carro")
                        .HasForeignKey("id_Carro")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("final_plataforma.Models.Producto", "producto")
                        .WithMany("producto_carro")
                        .HasForeignKey("id_Producto")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("final_plataforma.Models.Producto_Compra", b =>
                {
                    b.HasOne("final_plataforma.Models.Compra", "compra")
                        .WithMany("producto_compra")
                        .HasForeignKey("id_compra")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("final_plataforma.Models.Producto", "producto")
                        .WithMany("producto_compra")
                        .HasForeignKey("id_producto")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("final_plataforma.Models.Usuario_Compra", b =>
                {
                    b.HasOne("final_plataforma.Models.Compra", "compra")
                        .WithMany("usuario_compra")
                        .HasForeignKey("id_compra")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("final_plataforma.Models.Usuario", "usuario")
                        .WithMany("usuario_compra")
                        .HasForeignKey("id_usuario")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
