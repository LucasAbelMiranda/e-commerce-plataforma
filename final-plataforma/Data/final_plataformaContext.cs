﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using final_plataforma.Models;

namespace final_plataforma.Data
{
    public class final_plataformaContext : DbContext
    {
        public DbSet<Usuario> usuarios { get; set; }

        public DbSet<Categoria> categorias { get; set; }

        public DbSet<Producto> producto { get; set; }

        public DbSet<Compra> compras { get; set; }

        public DbSet<Carro> carro { get; set; }

        public DbSet<Producto_Carro> producto_carro { get; set; }

        public DbSet<Usuario_Compra> usuario_compra { get; set; }

        public DbSet<Producto_Compra> producto_compra { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"data source=localhost\SQLEXPRESS;initial catalog=final_plataforma;trusted_connection=true");
        }


        public final_plataformaContext (DbContextOptions<final_plataformaContext> options)
            : base(options)
        {
        }

        public DbSet<final_plataforma.Models.Usuario> Usuario { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categoria>()
                .ToTable("categorias")
                .HasKey(C => C.id);


            modelBuilder.Entity<Producto>()
                .ToTable("producto")
                .HasKey(P => P.id);

            modelBuilder.Entity<Producto>()
                .HasOne(P => P.categoria)
                .WithMany(C => C.productos)
                .HasForeignKey(C => C.id_categoria);


            modelBuilder.Entity<Usuario>()
                .ToTable("usuarios")
                .HasKey(U => U.id);
            modelBuilder.Entity<Usuario>()
                .HasOne(U => U.carro)
                .WithOne(Ca => Ca.usuario)
                .HasForeignKey<Carro>(Ca => Ca.usuario_id);



            //tabla intermedia usuario_compra
            modelBuilder.Entity<Usuario_Compra>()
                .ToTable("usuario_compra")
                .HasKey(UC => UC.id);


            modelBuilder.Entity<Usuario_Compra>()
                .HasOne(UC => UC.usuario)
                .WithMany(U => U.usuario_compra)
                .HasForeignKey(up => up.id_usuario);


            modelBuilder.Entity<Usuario_Compra>()
                .HasOne(UC => UC.compra)
                .WithMany(C => C.usuario_compra)
                .HasForeignKey(UC => UC.id_compra);


            //tabla carro
            modelBuilder.Entity<Carro>()
                .ToTable("carro")
                .HasKey(Ca => Ca.id);


            //tabla compra
            modelBuilder.Entity<Compra>()
               .ToTable("compras")
               .HasKey(Co => Co.id);

            //Tabla intermedia carro_poducto
            modelBuilder.Entity<Producto_Carro>() 
              .ToTable("producto_carro") 
               .HasKey(pc => pc.id); 



            modelBuilder.Entity<Producto_Carro>()
                .HasOne(pc => pc.producto)
                .WithMany(p => p.producto_carro)
                .HasForeignKey(pc => pc.id_Producto);


            modelBuilder.Entity<Producto_Carro>()
                .HasOne(pc => pc.carro)
                .WithMany(Ca => Ca.producto_Carro)
                .HasForeignKey(pc => pc.id_Carro);

            /* modelBuilder.Entity<Compra>()
                 .HasOne(Co => Co.comprador)
                 .WithMany(U => U.compras);
            */


            //tabla intermedia producto_compra
            modelBuilder.Entity<Producto_Compra>()
                .ToTable("producto_compra")
                .HasKey(PC => PC.id);


            modelBuilder.Entity<Producto_Compra>()
                .HasOne(PC => PC.producto)
                .WithMany(U => U.producto_compra)
                .HasForeignKey(PC => PC.id_producto);


            modelBuilder.Entity<Producto_Compra>()
                .HasOne(PC => PC.compra)
                .WithMany(C => C.producto_compra)
                .HasForeignKey(PC => PC.id_compra);



            modelBuilder.Entity<Usuario>().HasData(
                new { id = 1, dni = 123, nombre = "cliente", apellido = "apellido", mail = "mail", password = "pass", tipo = "cliente", cuil = "123", id_carro = 1,habilitado = true, intentos = 3 },
                new { id = 2, dni = 321, nombre = "admin", apellido = "apellido", mail = "mail", password = "pass", tipo = "admin", cuil = "423", id_carro = 2, habilitado = true , intentos = 3 });

            modelBuilder.Entity<Carro>().HasData(
           new { id = 1, usuario_id = 1 },
           new { id = 2, usuario_id = 2 });

            modelBuilder.Entity<Categoria>().HasData(
            new { id = 1, nombre = "electro" },
           new { id = 2, nombre = "deco" },
           new { id = 3, nombre = "varios" });

            modelBuilder.Entity<Producto>().HasData(
               new { id = 1, nombre = "tv", precio = 100.0, cantidad = 200, id_categoria = 1 },
               new { id = 2, nombre = "radio", precio = 150.0, cantidad = 300, id_categoria = 3 },
               new { id = 3, nombre = "silla", precio = 100.0, cantidad = 200, id_categoria = 2 });


        }

    }


}
